var blessed = require('blessed'),
    OpenNebula = require('opennebula');

var options = require("./options");
var tools = require("./blessedTools");
//var one = new OpenNebula('oneadmin:d82a2be86f29a3b5e663323986f4a358', 'http://192.168.1.240:2633/RPC2');
var one = new OpenNebula('oneadmin:opennebula', 'http://localhost:2633/RPC2');

var listVms = [], listHosts = [];
var som;
var live;
var id, idHost;
var constUpdate;
// Create a screen object.
var screen= blessed.screen({
    smartCSR: true,
    title: 'Computação na Nuvem MEI - Internet of Things'
});

var firstBox = tools.boxBlessed(options.first(screen));
var boxM = tools.boxBlessed(options.forms);
var mainMenu = tools.listBlessed(options.listSettings);
var menuHosts = tools.listBlessed(options.listSettings);
var menuVm = tools.listBlessed(options.listSettings);
var opcVM = tools.listBlessed(options.listSettings);
var opcHosts = tools.listBlessed(options.listSettings);
var menuCreate = tools.listBlessed(options.listSettings);
var info = tools.boxBlessed(options.popup);
var infoHost = tools.boxBlessed(options.popup);
var form = blessed.form(options.forms);

var txtName = tools.blessedTxtBox(form, 5, 2);
var txtRam = tools.blessedTxtBox(form, 5, 6);
var txtVcpu = tools.blessedTxtBox(form, 5, 10);

boxM.append(form);
firstBox.append(mainMenu);
firstBox.append(tools.appendText(0, 2, null, 'Trabalho Realizado por:'));
firstBox.append(tools.appendText(2, 3, null, 'Rui Barcelos,   18834'));
firstBox.append(tools.appendText(2, 4, null, 'David Bernardo, 18956'));
firstBox.append(tools.appendText(2, null, 0, 'ESQ - Sair          enter - Selecionar          v - Voltar '));
// Text boxes
boxM.append(tools.appendText(5, 1, null, 'NAME:'));
boxM.append(tools.appendText(5, 5, null, 'RAM:'));
boxM.append(tools.appendText(5, 9, null, 'VCPU:'));

function init() {
    one.version(function (err, data) {
        if (!err) {
            searchVMs();
            searchHosts();
            options.mainMenuPopulate(screen, mainMenu, options.menuUm);
            optionMenuVM()
            optionMenuHost();
            menuCreate.addItem("Criar VM (Pré-definida)");
            menuCreate.addItem("Criar VM (Parametrizada)");
        }else{
            popupMain("Erro de conexão"+err);
            setInterval(()=>{
                process.exit(0);
            },3000);
        }
        
    });

}
init();

//procurar máquinas Virtuais
function searchVMs() {
    one.getVMs((err, data) => {
        if (!err) {
            if (data[0] != null) {
                listVms = options.saveToArray(data, listVms);
            } else {
                popupMain("Não existem VMs");
            }
        } else {
            //popupMain("Erro de conexão: " + err)
        }
        console.log();
    });
}
//procurar máquinas Virtuais
function searchHosts() {
    one.getHosts(function (err, data) {
        if (!err) {
            if (data.length > 0) {
                listHosts = options.saveToArray(data, listHosts);
            } else {
                popupMain("Não existem Hosts")
            }
        } else {
            //popupMain("Erro de conexão: " + err)
        }
    });
}

//executa as funções das máquinas
function bootVM(type, idm) {
    var vm = one.getVM(parseInt(idm));
    vm.action(options.optVM[type].cmd, function (err, data) {
        if (!err) {
            popupOPC("Máquina " + data + " está em estado " + options.optVM[type].name);
        }
        else {
            popupOPC("Não pode selecionar este estado: " + err);
        }
    });
}

//executa a info das máquinas
var infoVM = (idm) => {
    constUpdate = setInterval(() => {
        var vm = one.getVM(parseInt(idm));
        vm.info((err, data) => {
            info.append(tools.appendText(0, 0, null, "ID:      " + data.VM.ID + "                                                  "));
            info.append(tools.appendText(0, 1, null, "NAME:    " + data.VM.NAME + "                                                "));
            info.append(tools.appendText(0, 2, null, "CPU:     " + data.VM.MONITORING.CPU + "%/" + data.VM.TEMPLATE.CPU + "vCPU(s)     "));
            info.append(tools.appendText(0, 3, null, "MEMORY:  " + (parseInt(data.VM.MONITORING.MEMORY) / 1024).toFixed(2) + "MB/" + data.VM.TEMPLATE.MEMORY + "MB  "));
            info.append(tools.appendText(0, 4, null, "NET RX:  " + data.VM.MONITORING.NETRX + "B                                   "));
            info.append(tools.appendText(0, 5, null, "NET TX:  " + data.VM.MONITORING.NETTX + "B                                   "));
            info.append(tools.appendText(0, 6, null, "State:   " + options.stateVM(data.VM.STATE) + "                                "));
            info.append(tools.appendText(0, 7, null, "LCMState:" + options.lcmStateVM(data.VM.LCM_STATE) + "                        "));
            opcVM.append(info);
            info.focus();
            screen.render();
            //console.log(data.VM);
        })
    }, 100);
}
//executa as funções das máquinas
var infoH = (idm) => {
    constUpdate = setInterval(() => {
        var host = one.getHost(parseInt(idm));
        host.info(function (err, data) {
            infoHost.append(tools.appendText(0, 0, null, "ID:         " + data.HOST.ID + "                                                   "));
            infoHost.append(tools.appendText(0, 1, null, "NAME:       " + data.HOST.NAME + "                                                 "));
            infoHost.append(tools.appendText(0, 2, null, "CPU:        " + data.HOST.HOST_SHARE.CPU_USAGE + "% usados em " + data.HOST.HOST_SHARE.TOTAL_CPU + "%       "));
            infoHost.append(tools.appendText(0, 3, null, "MEMORY:     " + (parseInt(data.HOST.HOST_SHARE.MEM_USAGE) / 1024) + "MB Usados em " + (parseInt(data.HOST.HOST_SHARE.TOTAL_MEM) / 1024).toFixed(2) + "MB  "));
            infoHost.append(tools.appendText(0, 4, null, "HYPERVISOR: " + data.HOST.TEMPLATE.HYPERVISOR + "                                  "));
            infoHost.append(tools.appendText(0, 5, null, "MODEL NAME: " + data.HOST.TEMPLATE.MODELNAME + "                                   "));
            infoHost.append(tools.appendText(0, 6, null, "NUMBER VMs: " + data.HOST.HOST_SHARE.RUNNING_VMS + "                               "));
            opcHosts.append(infoHost);
            infoHost.focus();
            screen.render();
        })
    }, 100);
}


//popula o menu com as funções de execução
function optionMenuVM() {
    opcVM.clearItems();
    for (var i = 0; i < options.optVM.length; i++) {
        opcVM.addItem(options.optVM[i].name)
    }
    opcVM.addItem("Informação da VM");
    opcVM.addItem("Migrate VM");
    opcVM.addItem("Migrate VM LIVE");
}
//popula o menu com as funções de execução
function optionMenuHost() {
    opcHosts.clearItems();
    opcHosts.addItem("Informação do Host");
}
//mostra o aviso de opção executada
var popupOPC = (message) => {
    var box = tools.boxBlessed(options.popup);
    box.append(tools.appendText(1, 0, null, message));
    //Mostra POPUP
    opcVM.append(box);
    screen.render();
    //Fechar o POPUP
    setInterval(() => {
        opcVM.remove(box);
        screen.render();
    }, 3000);
}
//Caso Exista erro na listagem de máquinas mostra uma mensagem
var popupMain = (message) => {
    var box = tools.boxBlessed(options.popup);
    box.append(tools.appendText(1, 0, null, message));
    //Mostra POPUP
    mainMenu.append(box);
    screen.render();
    //Fechar o POPUP
    setInterval(function () {
        mainMenu.remove(box);
        screen.render();
    }, 3000);
}

//mostra o aviso de máquina criada
var popupCrt = (message) => {
    var box = tools.boxBlessed(options.popup);
    box.append(tools.appendText(1, 0, null, message));
    //Mostra POPUP
    menuCreate.append(box);
    screen.render();
    //Fechar o POPUP
    setInterval(function () {
        menuCreate.remove(box);
        screen.render();
    }, 3000);
}
//mostra o aviso de opção executada
var popupMigrate = (message) => {
    var box = tools.boxBlessed(options.popup);
    box.append(tools.appendText(1, 0, null, message));
    //Mostra POPUP
    menuHosts.append(box);
    screen.render();
    //Fechar o POPUP
    setInterval(function () {
        menuHosts.remove(box);
        screen.render();
    }, 3000);
}
//Mostra o menu create
function showMenuCreate() {
    mainMenu.append(menuCreate);
    menuCreate.focus();
    screen.render();
}

//criar máquina Virtual
var createVM = (name, ram, vcpu) => {
    var vma = 'CONTEXT = [  NETWORK = "YES", SSH_PUBLIC_KEY = "root[SSH_PUBLIC_KEY]" ] \n CPU = "' + (vcpu || 0.1) + '"\n  MEMORY = "' + (ram || 128) + '"\n NIC = [ NETWORK = "cloud" ]\n';
    var template = one.getTemplate(0);
    var host = one.getHost(0);
    template.instantiate(name, undefined, vma, function (err, vm) {
        vm.deploy(host.id, 0, true, () => {
            if (!err) {
                popupCrt("VM " + name + " Criada!");
                searchVMs();
                options.populateLists(menuVm, listVms);
            } else {
                popupCrt("Erro ao criar" + err);
            }
        });

    });

}

//Selecionar no mainMenu
mainMenu.key('enter', function () {
    var item = mainMenu.getItemIndex(mainMenu.selected);
    if (item == 0) {
        if (listVms.length > 0) {
            options.populateLists(menuVm, listVms);
            mainMenu.append(menuVm);
            menuVm.focus();
            screen.render();
        } else {
            popupMain("Não existem VMs! Crie uma!!!")
        }
    } else if (item == 1) {
        showMenuCreate();
    } else if (item == 2) {
        if (listHosts.length > 0) {
            options.populateLists(menuHosts, listHosts);
            som = 0;
            mainMenu.append(menuHosts);
            menuHosts.focus();
            screen.render();
        } else {
            popupMain("Não existem Hosts! Crie um!!!")
        }
    } else {
        return process.exit(0);
    }
    screen.render();
});

//Selecionar no Menu para Criar VMS
menuCreate.key('enter', function () {
    var item = menuCreate.getItemIndex(menuCreate.selected);
    if (item == 0) {
        createVM("vmTeste", "128", "0.1");
    } else if (item == 1) {
        menuCreate.append(boxM);
        form.focus();
    } else {
        return process.exit(0);
    }
    screen.render();
});

// Quit on Escape, q, or Control-C.
screen.key(['escape', 'q', 'C-c'], function (ch, key) {
    return process.exit(0);
});
//do menu das maquina virtuais para o das opções 
menuVm.key('enter', function () {
    if (listVms.length > 0) {
        var machine = menuVm.getItemIndex(menuVm.selected);
        id = listVms[machine].id
        if (listVms.length > machine) {
            menuVm.append(opcVM);
            opcVM.focus();
        }
        screen.render();
    } else {
        mainMenu.remove(menuVm);
        mainMenu.focus();
        screen.render();
    }

});

//do menu dos Hosts para o das opções 
menuHosts.key('enter', function () {
    var machine = menuHosts.getItemIndex(menuHosts.selected);
    if (listHosts.length > machine) {
        idHost = listHosts[machine].id
        if (som == 0) {
            menuHosts.append(opcHosts);
            opcHosts.focus();
        } else {
            var vm = one.getVM(parseInt(id));
            var host = one.getHost(parseInt(idHost));
            vm.migrate(parseInt(host.id), live, true, 0, (err, data) => {
                if (!err) {
                    if (live == true) {
                        popupMigrate("Máquina " + id + " foi migrada, em Live para o HOST " + idHost);
                    } else {
                        popupMigrate("Máquina " + id + " foi migrada para o HOST " + idHost);
                    }
                } else {
                    popupMigrate("Houve um erro: " + err);
                }
            });
        }
    }
    screen.render();
});

//acção a realizar na máquina selecionada no menu anterior
opcHosts.key('enter', function () {
    infoH(idHost)
    screen.render();
});
opcVM.key('enter', function () {
    som = 1;
    var machine = opcVM.getItemIndex(opcVM.selected);
    if (options.optVM.length > machine) {
        bootVM(machine, id);
    } else if (options.optVM.length == machine) {
        infoVM(id)
    } else if ((options.optVM.length + 1) == machine) {
        live = false;
        options.populateLists(menuHosts, listHosts);
        opcVM.append(menuHosts);
        menuHosts.focus();
        screen.render();
    } else if ((options.optVM.length + 2) == machine) {
        live = true;
        options.populateLists(menuHosts, listHosts);
        opcVM.append(menuHosts);
        menuHosts.focus();
        screen.render();
    }
    screen.render();
});
//parar o setInterval das estatisticas
var stopConstUpdate = () => {
    clearInterval(constUpdate);
}
//voltar às opções das máquinas virtuais
info.key('v', function () {
    stopConstUpdate();
    opcVM.remove(info);
    opcVM.focus();
    screen.render();
});
infoHost.key('v', function () {
    stopConstUpdate();
    opcHosts.remove(infoHost);
    opcHosts.focus();
    screen.render();
});
//voltar à lista de máquinas virtuais
opcVM.key('v', function () {
    searchVMs();
    options.populateLists(menuVm, listVms);
    menuVm.remove(opcVM);
    menuVm.focus();
    screen.render();
});
//voltar à lista de Hosts
opcHosts.key('v', function () {
    searchHosts();
    options.populateLists(menuHosts, listHosts);
    menuHosts.remove(opcHosts);
    menuHosts.focus();
    screen.render();
});
// Voltar ao Main Menu do menu das máquinas virtuais
menuVm.key('v', function () {
    mainMenu.remove(menuVm);
    mainMenu.focus();
    screen.render();
});
// Voltar ao Main Menu do menu dos Hosts
menuHosts.key('v', function () {
    if (som == 0) {
        mainMenu.remove(menuHosts);
        mainMenu.focus();
    } else {
        opcVM.remove(menuHosts);
        opcVM.focus();
    }
    screen.render();
});
// Voltar ao Main Menu do menu de criar máquinas virtuais
menuCreate.key('v', function () {
    mainMenu.remove(menuCreate);
    mainMenu.focus();
    screen.render();
});
//No form voltar ao menu criar máquinas virtuais
form.key('v', function () {
    menuCreate.remove(boxM);
    menuCreate.focus();
    screen.render();
});
//Botão Submeter Formulário
var submit = blessed.button(options.buttons(form));
//Função de Submeter o formulário
submit.on('press', function () {
    form.submit();
});
//buscar os dados do formulário 
form.on('submit', function (data) {
    createVM(data.crt[0], data.crt[1], data.crt[2]);
    setInterval(function () {
        menuCreate.remove(boxM);
        menuCreate.focus();
        screen.render();
    }, 5000);
});
mainMenu.focus();
screen.render();