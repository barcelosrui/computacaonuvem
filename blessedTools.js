var blessed = require('blessed');

exports.boxBlessed = (boxType)=> {
    return blessed.box(boxType);
}

exports.listBlessed = (listType) => {
    return blessed.list(listType);
}

//Criar textBox
exports.blessedTxtBox = (par,leftW, topW) => {
    return blessed.textbox({
        parent: par,
        name: 'crt',
        top: topW,
        left: leftW,
        height: 3,
        inputOnFocus: true,
        content: 'first',
        border: {
            type: 'line'
        },
        focus: {
            fg: 'blue'
        }
    });
}
//Função genérica para inserir texto nas boxes
exports.appendText = (leftW, topW, bottomW, textC) => {
    return new blessed.Text({
        left: leftW,
        top: topW,
        bottom: bottomW,
        content: textC,
        style: {
            fg: 'white',
            bg: 'blue',
            border: {
                fg: '#f0f0f0'
            },
            hover: {
                bg: 'green'
            }
        },
    })
}