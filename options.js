var blessed = require('blessed');
exports.first = function (par) {
    return {
        parent: par,
        style: {
            fg: 'white',
            bg: 'blue',
            border: {
                fg: '#f0f0f0'
            },
            hover: {
                bg: 'green'
            }
        },
        border: {
            type: 'line',
            fg: '#ffffff'
        },
        tags: true,
        content: '{center}Projeto Final CNV{/center}',
        width: '100%',
        height: '100%',
        top: 'center',
        left: 'center'
    }
}

exports.listSettings = {
    style: {
        fg: 'white',
        bg: 'green',
        border: {
            fg: '#f0f0f0'
        },
        hover: {
            bg: 'green'
        }
    },
    width: '75%',
    height: '75%',
    top: 'center',
    left: 'center',
    align: 'LEFT',
    fg: 'blue',
    border: {
        type: 'line'
    },
    selectedBg: 'red',
    // Allow key support (arrow keys + enter)
    keys: true,

    // Use vi built-in keys
    vi: true
}
exports.forms = {
    width: '90%',
    left: 'center',
    keys: true,
    vi: true
}
exports.popup = {
    top: 'center',
    left: 'center',
    width: '75%',
    height: '75%',
    tags: true,
    border: {
        type: 'line'
    },
    style: {
        fg: 'white',
        bg: 'blue',
        border: {
            fg: '#f0f0f0'
        },
        hover: {
            bg: 'green'
        }
    }
}

exports.menuUm = ["Listar VMs", "Criar Vms", "Listar Hosts", "Sair"];

exports.optVM = [
    { name: "Iniciar", cmd: "resume" },
    { name: "Reiniciar (normal)", cmd: "reboot" },
    { name: "Reiniciar (forçado)", cmd: "reboot_hard" },
    { name: "Desligar (normal)", cmd: "poweroff" },
    { name: "Desligar (forçado)", cmd: "poweroff_hard" },
    { name: "Destruir (normal)", cmd: "terminate" }
];

var state = [
    { id: -2, state: "Any state, including DONE" },
    { id: -1, state: "Any state, except DONE" },
    { id: 0, state: "INIT" },
    { id: 1, state: "PENDING" },
    { id: 2, state: "HOLD" },
    { id: 3, state: "ACTIVE" },
    { id: 4, state: "STOPPED" },
    { id: 5, state: "SUSPENDED" },
    { id: 6, state: "DONE" },
    { id: 7, state: "FAILED" },
    { id: 8, state: "POWEROFF" },
    { id: 9, state: "UNDEPLOYED" },
    { id: 10, state: "CLONING" },
    { id: 11, state: "CLONING_FAILURE" }
];
//mostrar o estado da VM
exports.stateVM = (idState) => {
    var value = "";
    for (let index = 0; index < state.length; index++) {
        if (state[index].id == parseInt(idState)) {
            value = state[index].state
        }
    }
    return value;
}
var lcmState = [
    { id: -2, state: "Any state, including DONE" },
    { id: -1, state: "Any state, except DONE" },
    { id: 0, state: "LCM_INIT" },
    { id: 1, state: "PROLOG" },
    { id: 2, state: "BOOT" },
    { id: 3, state: "RUNNING" },
    { id: 4, state: "MIGRATE" },
    { id: 5, state: "SAVE_STOP" },
    { id: 6, state: "SAVE_SUSPEND" },
    { id: 7, state: "SAVE_MIGRATE" },
    { id: 8, state: "PROLOG_MIGRATE" },
    { id: 9, state: "PROLOG_RESUME" },
    { id: 10, state: "EPILOG_STOP" },
    { id: 11, state: "EPILOG" },
    { id: 12, state: "SHUTDOWN" },
    { id: 13, state: "SHUTDOWN" },
    { id: 14, state: "SHUTDOWN" },
    { id: 15, state: "CLEANUP_RESUBMIT" },
    { id: 16, state: "UNKNOWN" },
    { id: 17, state: "HOTPLUG" },
    { id: 18, state: "SHUTDOWN_POWEROFF" },
    { id: 19, state: "BOOT_UNKNOWN" },
    { id: 20, state: "BOOT_POWEROFF" },
    { id: 21, state: "BOOT_SUSPENDED" },
    { id: 22, state: "BOOT_STOPPED" },
    { id: 23, state: "CLEANUP_DELETE" },
    { id: 24, state: "HOTPLUG_SNAPSHOT" },
    { id: 25, state: "HOTPLUG_NIC" },
    { id: 26, state: "HOTPLUG_SAVEAS" },
    { id: 27, state: "HOTPLUG_SAVEAS_POWEROFF" },
    { id: 28, state: "HOTPLUG_SAVEAS_SUSPENDED" },
    { id: 29, state: "SHUTDOWN_UNDEPLOY" },
    { id: 30, state: "EPILOG_UNDEPLOY" },
    { id: 31, state: "PROLOG_UNDEPLOY" },
    { id: 32, state: "BOOT_UNDEPLOY" },
    { id: 33, state: "HOTPLUG_PROLOG_POWEROFF" },
    { id: 34, state: "HOTPLUG_EPILOG_POWEROFF" },
    { id: 35, state: "BOOT_MIGRATE" },
    { id: 36, state: "BOOT_FAILURE" },
    { id: 37, state: "BOOT_MIGRATE_FAILURE" },
    { id: 38, state: "PROLOG_MIGRATE_FAILURE" },
    { id: 39, state: "PROLOG_FAILURE" },
    { id: 40, state: "EPILOG_FAILURE" },
    { id: 41, state: "EPILOG_STOP_FAILURE" },
    { id: 42, state: "EPILOG_UNDEPLOY_FAILURE" },
    { id: 43, state: "PROLOG_MIGRATE_POWEROFF" },
    { id: 44, state: "PROLOG_MIGRATE_POWEROFF_FAILURE" },
    { id: 45, state: "PROLOG_MIGRATE_SUSPEND" },
    { id: 46, state: "PROLOG_MIGRATE_SUSPEND_FAILURE" },
    { id: 47, state: "BOOT_UNDEPLOY_FAILURE" },
    { id: 48, state: "BOOT_STOPPED_FAILURE" },
    { id: 49, state: "PROLOG_RESUME_FAILURE" },
    { id: 50, state: "PROLOG_UNDEPLOY_FAILURE" },
    { id: 51, state: "DISK_SNAPSHOT_POWEROFF" },
    { id: 52, state: "DISK_SNAPSHOT_REVERT_POWEROFF" },
    { id: 53, state: "DISK_SNAPSHOT_DELETE_POWEROFF" },
    { id: 54, state: "DISK_SNAPSHOT_SUSPENDED" },
    { id: 55, state: "DISK_SNAPSHOT_REVERT_SUSPENDED" },
    { id: 56, state: "DISK_SNAPSHOT_DELETE_SUSPENDED" },
    { id: 57, state: "DISK_SNAPSHOT" },
    { id: 58, state: "DISK_SNAPSHOT" },
    { id: 59, state: "DISK_SNAPSHOT_DELETE" },
    { id: 60, state: "PROLOG_MIGRATE_UNKNOWN" },
    { id: 61, state: "PROLOG_MIGRATE_UNKNOWN_FAILURE" }

];
//mostrar o estado da VM
exports.lcmStateVM = (idState) => {
    var value = "";
    for (let index = 0; index < lcmState.length; index++) {
        if (lcmState[index].id == parseInt(idState)) {
            value = lcmState[index].state
        }
    }
    return value;
}
//Populate first menu
exports.mainMenuPopulate = (scr, par, menu) => {
    for (var i = 0; i < menu.length; i++) {
        par.addItem(menu[i])
    }
    par.focus();
    scr.render();
}

//Função generica de passar da api para um array (Usado para guardar os dados dos hosts ou vms para dentro de arrays)
exports.saveToArray = (oldData, newData) => {
    newData = [];
    for (var i = 0; i < oldData.length; i++) {
        newData[i] = { id: oldData[i].ID, name: oldData[i].NAME };
    };
    return newData;
}
//Função generica para popular a listas (Usado para inserir os dados nos menus de vms e hosts)
exports.populateLists = (menu, list) => {
    menu.clearItems();
    for (var i = 0; i < list.length; i++) {
        menu.addItem(list[i].id + "-" + list[i].name);
    }
}
//Template de um botão
exports.buttons = (par) => {
    return {
        parent: par,
        name: 'submit',
        content: 'Submeter',
        top: 13,
        left: 5,
        shrink: true,
        padding: {
            top: 1,
            right: 2,
            bottom: 1,
            left: 2
        },
        style: {
            bold: true,
            fg: 'white',
            bg: 'green',
            focus: {
                inverse: true
            }
        }
    }
}